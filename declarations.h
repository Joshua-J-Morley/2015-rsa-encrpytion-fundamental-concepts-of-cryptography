#ifndef DECLR
#define DECLR

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdint.h>

#define LLI unsigned long long int

typedef struct PublicKey
{
	LLI p;
	LLI q;
	LLI n;
	LLI phi;
	LLI e;
}PublicKey;

typedef struct PrivateKey
{
	LLI n;
	LLI d;
}PrivateKey;


typedef struct CipherText
{
	LLI *text;
	int length;
}CipherText;

#endif