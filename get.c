#include "get.h"

void getPQ(PublicKey *pubKey)
{
	//loop until randomly recieved number is prime
	do
	{
		//get a random number between 1000 and 10,000
		pubKey->p = ((rand() % (9000)) + 1000);
	} while ((isPrime(pubKey->p) == 0));

	//loop until randomly recieved number is prime
	do
	{
		//get a random number between 1000 and 10,000
		pubKey->q = ((rand() % (9000)) + 1000);
	} while ((isPrime(pubKey->q) == 0) || pubKey->q == pubKey->p);

	//fprintf(stdout, "p is %lld\n", pubKey->p);
	//fprintf(stdout, "q is %lld\n", pubKey->q);
}

void getN(PublicKey *pubKey)
{
	//ensure that p and q are coprime
	//assert(greatestCommonFactor(pubKey->p, pubKey->q) == 1);

	//get value for n
	pubKey->n = pubKey->p * pubKey->q;

	//fprintf(stdout, "n is %lld\n", pubKey->n);
}

void getPhi(PublicKey *pubKey)
{
	pubKey->phi = (pubKey->p - 1) * (pubKey->q - 1);

	//fprintf(stdout, "phi is %lld\n", pubKey->phi);
}

void getE(PublicKey *pubKey)
{
	//loop until e is coprime to phi
	do
	{
		//generate random number from 1 to phi length
		pubKey->e = rand() % (pubKey->phi - 1) + 1;
	} while (greatestCommonFactor(pubKey->phi, pubKey->e) != 1);

	//fprintf(stdout, "e is %lld\n", pubKey->e);
}

PrivateKey* getD(PublicKey *pubKey)
{
	//initialise private key variable
	PrivateKey *privKey = malloc(sizeof(PrivateKey));

	//calculate d by finding modular inverse of e ^ phi
	privKey->d = moduloInverse(pubKey->e, pubKey->phi);

	//fprintf(stdout, "d is %lld\n", privKey->d);

	//return private key variable
	return privKey;
}

LLI moduloInverse(LLI a, LLI n)
{
	//variables to use in method
	LLI t, nextT, r, nextR, quotent, temp;

	//initialise t and next t values
	t = 0;
	nextT = 1;

	//set remainder to power value
	r = n;
	//set next R to base value
	nextR = a;

	//loop until remainder is 0
	while (nextR != 0)
	{
		//set quotent for iteration
		quotent = r / nextR;

		//temprary variable to store t
		temp = t;

		//iterate t value
		t = nextT;

		//set next t to current t value - quotent*next t
		nextT = temp - (quotent * nextT);

		//temp variable to store r
		temp = r;

		//remainder = next remainder
		r = nextR;

		//next remainder = current r - quotent*next remainder
		nextR = temp - (quotent * nextR);

		//if t is now negative
		if (t < 0)
		{
			//add n to modular inverse running number
			t = t + n;
		}
	}

	return t;
}

int isPrime(LLI num)
{
	//fprintf(stdout, "\n");
	if (num < 2)
	{
		return 0;
	}
	if (num == 2)
	{
		return 1;
	}
	for (int ii = 0; ii < 25; ii++)
	{
		LLI a = (rand() % (num - 1)) + 1;

		LLI temp = moduloPower(a, (num - 1) / 2, num);

		if ((temp % num != 1) && (temp % num != (num - 1)))
		{
			return 0;
		}
	}
	return 1;
}


//aa^bb mod nn
/*LLI moduloPower(LLI aa, LLI bb, LLI nn)
{
	LLI xAtii, retVal;
	retVal = 1;
	//fprintf(stdout, "modpower m:%lld\tbb:%lld\tn:%lld\t", m, bb, n);

	for (int ii = 64; ii >= 0; ii--)
	{
		//xAtii = ((bb) & (LLI)(((LLI)1 << (ii))/pow(2, ii)));
		bii = (bb) & (LLI)((1 << ii) / (pow(2, ii)));
		//fprintf(stdout, "bii:%lld\t", bii);

		retVal = (retVal * retVal) % nn;

		if ((bii == 1))
		{
			retVal = (retVal * aa) % nn;
		}
	}
	//fprintf(stdout, "retval:%lld\n", retVal);

	return retVal;
}*/

LLI moduloPower(LLI base, LLI exp, LLI modulus) 
{
	base %= modulus;
	LLI result = 1;

	//for every bit
	while (exp > 0) 
	{
		//if this bit is a 1
		if (exp & 1) 
		{
			//set result to running result * base %mod
			result = (result * base) % modulus;
		}
		//square base and mod by modulus
		base = (base * base) % modulus;

		//shift to next bit
		exp >>= 1;
	}
	//return final modular power number
	return result;
}

LLI greatestCommonFactor( LLI a, LLI b)
{
	LLI temp = 0;

	//loop until remainder of b/a is 0
	while (b != 0)
	{
		//temporary to store a
		temp = a;
		
		//set a to b value
		a = b;

		//get remainder of dividing b from initial a
		b = temp % b;
	}

	//return greatest common factor
	return a;
}
