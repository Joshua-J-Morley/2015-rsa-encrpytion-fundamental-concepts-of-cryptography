#include "declarations.h"


void getPQ(PublicKey *pubKey);


void getN(PublicKey *pubKey);


void getPhi(PublicKey *pubKey);

void getE(PublicKey *pubKey);

PrivateKey* getD(PublicKey *pubKey);


LLI moduloInverse(LLI e, LLI phi);

int isPrime(LLI num);

LLI moduloPower(LLI base, LLI exp, LLI modulus);

LLI greatestCommonFactor( LLI a, LLI b);
