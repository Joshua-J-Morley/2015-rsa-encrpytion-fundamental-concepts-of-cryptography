#include "inout.h"

char* getMessage()
{
	//create temporary array to get message
	char buffer[1001];
	char* message;

	//input message from user
	fprintf(stdout, "entermessage\n");
	fscanf(stdin, "%s", buffer);

	//allocate memory size of message
	message = malloc(strlen(buffer));
	
	//copy contents of message into char array
	strcpy(message, buffer);

	//return message
	return message;
}

char *readFile()
{
	FILE *file;
	char fileName[25], *retStr, character;
	int ii = 0;
	long size;

	//input filename from user
	fprintf(stdout, "Enter filename\n");
	fscanf(stdin, "%s", fileName);
	
	//open file to read
	file = fopen(fileName, "r");

	//if file does not open correctly indicate so
	if (ferror(file))
	{
		perror("error opening file\n");
	}
	//if file opens correctly
	else
	{
		fprintf(stdout, "opened successfully\n");

		//get length of file
		fseek(file, 0, SEEK_END);
		size = ftell(file);
		fseek(file, 0, SEEK_SET);

		//set the return char array to number of characters
		//inthe file
		retStr = malloc(size);

		//read in each character from file
		while ((character = fgetc(file)) != EOF)
		{
			//put character in array to return
			retStr[ii] = character;
			ii++;
		}
	}

	//return array of characters for plaintext
	return retStr;
}