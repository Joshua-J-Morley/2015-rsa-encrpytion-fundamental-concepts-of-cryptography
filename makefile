#make variables used in makefile
CC = gcc
CFLAGS = -g -Wall -std=c99 -lm
EXEC = run
SOURCES = $(wildcard *.c) 
OBJECTS = $(SOURCES:.c=.o)

# execute target
# run program
$(EXEC): compile
#	@make clean --no-print-directory
#	gcc -c $(CFLAGS) stuff.c
#	@$(CC) $(CFLAGS) stuff.o -o $(EXEC)
	@./run
#	./run test.txt

#compile target
# compile all object files into executable
# remove obselete object files
compile: $(OBJECTS)
#	@echo Linking
	@$(CC) $(CFLAGS) $(OBJECTS) -o $(EXEC)
#	@echo
#	@echo Cleaning up
#	@make clean --no-print-directory

# To obtain object files
# compile .c files using previously stated compiler flags
%.o: %.c
#	@make clean --no-print-directory
#	@clear
#	@echo Compiling $@
	@$(CC) -c $(CFLAGS) $< -o $@ 
#	@echo

# save compilation and execution commands and outputs of program to file
record:
	@script -a -c make log.txt


debug: compile
	@./run -DEBUG
# To remove generated files
clean:
	@rm -f $(OBJECTS) run