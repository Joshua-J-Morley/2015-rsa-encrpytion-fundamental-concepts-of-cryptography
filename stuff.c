#include "stuff.h"

int main(int argc, char const *argv[])
{
	//seed random number generator with current time
	srand(time(NULL));

	//perform modular exponentiation on 17^21 mod 39
	doModExpon();

	//perform RSA algorithm
	doRSA();

	return 0;
}

void doModExpon()
{
	//perform modular exponentiation and output result
	fprintf(stdout, "17^21 mod 39= ");
	fprintf(stdout, "%lld\n\n", moduloPower(17, 21, 39));
}

void doRSA()
{
	PrivateKey *privKey;
	CipherText *cipher = malloc(sizeof(CipherText));
	cipher->text = malloc(1);
	char *message, *retPlainText;

	//test RSA method with keyboard symbols
	message = testRSA1();

	//perform encryption on keyboard symbols
	privKey = encrypt(message, cipher);

	//perform decryption on keyboard symbols
	retPlainText = decrypt(cipher, privKey);
	
	printf("%lld",cipher->text[0]);

	//test RSA method with input from terminal
	//message = testRSA2();

	
	//test RSA method on textfile
	message = testRSA3();

	//perform encryption on input text
	privKey = encrypt(message, cipher);

	//perform decryption on input text
	retPlainText = decrypt(cipher, privKey);
	
}

char* testRSA1()
{
	//return all keyboard symbols as char array
	return "`1234567890-=qwertyuiop[]\asdfghjkl;'zxcvbnm,./ ~!@#$%^&*()_+{}|:<>?";
}

char* testRSA2()
{
	return getMessage();
}

char* testRSA3()
{
	return readFile();
}

PrivateKey* encrypt(char* message, CipherText *cipher)
{
	LLI character;
	PublicKey *pubKey = malloc(sizeof(PublicKey));
	PrivateKey *privKey;

	//get value of PQ
	getPQ(pubKey);

	//get value of N
	getN(pubKey);

	//get value of PHI
	getPhi(pubKey);

	//get value of E
	getE(pubKey);


	//get value of D
	privKey = getD(pubKey);
	//set n value in private key variable
	privKey->n = pubKey->n;

	//set length of cipher text as length of plaintext
	cipher->length = strlen(message);

	//trim the fat
	cipher->text = realloc(cipher->text, cipher->length * sizeof(LLI));

	//for every character in plaintext
	for (int ii = 0; ii < cipher->length; ii++)
	{
		//convert character to ascii value
		character = message[ii];

		//perform modular exponentiation on characters ascii value
		character = moduloPower(character, pubKey->e, pubKey->n);

		//add new value to cipher text
		cipher->text[ii] = character;
	}

	return privKey;
}

char* decrypt(CipherText *cipher, PrivateKey *privKey)
{
	LLI Lcharacter;
	char *plainText = malloc(cipher->length);
	char character;

	//for every character in ciphertext
	for (int ii = 0; ii < cipher->length; ii++)
	{
		//get number representing this character
		Lcharacter = cipher->text[ii];

		//perform modular power to reverse encryption
		Lcharacter = moduloPower(Lcharacter, privKey->d, privKey->n);

		//temporary character t store converted ascii number to char
		character = (int)Lcharacter;

		//add character to plaintext array
		plainText[ii] = character;
	}

	//print out deciphered text
	//fprintf(stdout, "\nr: %s\n", plainText);

	return plainText;
}







