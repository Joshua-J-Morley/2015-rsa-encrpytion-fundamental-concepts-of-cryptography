#include "declarations.h"
#include "get.h"
#include "inout.h"

/*
public key is n with e
private key is d
cipherText = m^e mod n
plainText = c^d mod n
*/

int main(int argc, char const *argv[]);

void doModExpon();
void doRSA();


char* testRSA1();
char* testRSA2();
char* testRSA3();

PrivateKey* encrypt(char* message, CipherText *cipher);


char* decrypt(CipherText *cipher, PrivateKey *privKey);
